Dot.NET Core Microservices
===

Contains notes from [Dot.NET Core Microservices](https://www.packtpub.com/application-development/net-core-microservices-video)

What we are building
---

Actio (from latin: actum + tio = do,make) app, consisting of:

- API: gateway
- Identity service: handling authentication and account
- Activities service: storing the activities data

Set-up environment (Mint 18.3)
---

Install **Dot.net Core**

Install Dot.net from Microsoft, example link to [Debian installation](https://www.microsoft.com/net/learn/get-started/linuxdebian).

Or install binaries from [Binaries](https://www.microsoft.com/net/download/thank-you/dotnet-sdk-2.1.4-linux-x64-binaries)

, using the following commands:

```bash
mkdir -p $HOME/dotnet && tar zxf dotnet.tar.gz -C $HOME/dotnet
export PATH=$PATH:$HOME/dotnet
```

Add the installation directory to the $PATH, add the above export to the `~/.bash_profile`.

Note: I installed version 2.1.4 at this time.

Install **Docker**

Installation notes [Here](https://tutorialforlinux.com/2018/01/08/how-to-install-docker-ce-on-linux-mint-18-3-64bit-step-by-step/)

Docker CE Repository, notes [Here](https://tutorialforlinux.com/how-to-quickstart-with-docker-ce-on-linux-distros/)

See:
[Docker web site](https://www.docker.com)

Install **MongoDB**

Import public GPG key:

```
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5
```

Create the “/etc/apt/sources.list.d/mongodb-org-3.6.list”:

```
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.6 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list
```

Then run:
```
sudo apt-get update
sudo apt-get install -y mongodb-org
```

Start MongoDB:

```
sudo service mongod start
```

Verify Installed:

```
cat /var/log/mongodb/mongod.log
...
waiting for connections on port 27017
```

Use the following commands to start/stop/restart:

```
sudo service mongod stop
sudo service mongod start
sudo service mongod restart
```

Use MongoDB:

```
mongo --host 127.0.0.1:27017
...
help
exit
```

Install **Postman**

Download binary from [here](https://www.getpostman.com/)

After extracting, create XFce menu entry like this:

```
atanas@atanas-desktop-01 ~/.local/share/applications $ cat Postman.desktop 
#!/usr/bin/env xdg-open
[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=Postman
Exec=~/Apps/Postman/Postman
Icon=~/Apps/Postman/resources/app/assets/icon.png
Categories=Application;Development;IDE
```

See: [Install app to XFce menu](http://xubuntugeek.blogspot.com/2011/12/add-items-to-xfce-applications-menu.html)

System Architecture
---

Following steps:

- Request comes to the API [Gateway] and is sent to [Service Bus]
- Two services are subscribed to particular types of messages
  - [Identity Service]
  - [Activities Service]: handles "add_activity" and produces "activity_added"

, then we may publish activity feed on the API

Projects structure:

- Actio.API  - own MongoDB database [CQRS]
- Actio.Common -> Messages
- Actio.Services.Identity - own MongoDB database
- Actio.Services.Activities - own MongoDB database
- Actio.Tests
- Actio.Tests.EndToEnd

Creating the Solution
---

We create directory for the solution:

```
mkdir Actio
cd Actio
```

We create solution:

```
dotnet new sln
```

Create directories for the code, and move to the src

```
mkdir src scripts tests
cd src
```

, and create the following projects:

```
dotnet new webapi -n Actio.Api
dotnet new webapi -n Actio.Services.Identity
dotnet new webapi -n Actio.Services.Activities
dotnet new classlib -n Actio.Common
```

Now we add references between the projects:

```
dotnet add Actio.Api/Actio.Api.csproj reference Actio.Common/Actio.Common.csproj
dotnet add Actio.Services.Identity/Actio.Services.Identity.csproj reference Actio.Common/Actio.Common.csproj
dotnet add Actio.Services.Activities/Actio.Services.Activities.csproj reference Actio.Common/Actio.Common.csproj
```

And then include all the projects to the solution:

```
cd ..
dotnet sln add src/Actio.Api/Actio.Api.csproj
dotnet sln add src/Actio.Common/Actio.Common.csproj
dotnet sln add src/Actio.Services.Identity/Actio.Services.Identity.csproj
dotnet sln add src/Actio.Services.Activities/Actio.Services.Activities.csproj
```

Restore all the requited Nuget packages:

```
dotnet restore
```

And build the solution:

```
dotnet build
```

Configure `RabbitMQ`
---

We will be sending 2 types of events:

- commands
- events

We can run the RabbitMQ using Docker. Run docker and expose the RabbitMQ port:

```
sudo docker run -p 5672:5672 rabbitmq
```

Creating Commands
---

Looking at:

- Commands that will be used in the app
- The **Command handler pattern**

Creating Events
---

Looking at:

- Events that will be used in the app
- The **Event handler pattern**

Helpers to subscribe to service bus
---

In this part we:

- Create custom service host using Fluent API
- Create helper classes and extension methods

Add the following nuget packages to the Actio.Common:

```
dotnet add package Microsoft.AspNetCore
dotnet add package Microsoft.AspNetCore.Hosting
dotnet add package RawRabbit --version 2.0.0-rc3
dotnet add package RawRabbit.DependencyInjection.ServiceCollection --version 2.0.0-rc3
dotnet add package RawRabbit.Operations.Publish --version 2.0.0-rc3
dotnet add package RawRabbit.Operations.Subscribe --version 2.0.0-rc3
dotnet restore
```